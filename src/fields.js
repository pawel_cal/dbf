var fieldSize = require('./fieldsize');
var iconv = require('iconv-lite');

var types = {
	string: 'C',
	number: 'N',
	integer: 'N',
	real: 'F',
	boolean: 'L',
	// type to use if all values of a field are null
	null: 'C'
};

module.exports.multi = multi;
module.exports.bytesPer = bytesPer;
module.exports.obj = obj;

function multi(features, encode = 'utf-8') {
	var fields = {};
	features.forEach((feature) => inherit(fields, feature));
	return obj(fields, encode);
}

/**
 * @param {Object} fields
 * @param {Object} feature
 * @returns {Object}
 */
function inherit(fields, properties) {
	for (var key in properties) {
		var property = properties[key];
		var isDef = typeof property !== 'undefined' && property !== null;
		if (typeof fields[key] === 'undefined' || isDef) {
			if (typeof property === "number" && typeof fields[key] !== 'undefined') {
				if (Number.isInteger(fields[key]) && Number.isInteger(property)) {
					fields[key] = 1;
				} else {
					fields[key] = 1.1;
				}
			} else {
				fields[key] = property;
			}
		}
	}
	return fields;
}

function obj(_, encode = 'utf-8') {
	var fields = {},
	o = [];
	for (var key in _) {
		var property = _[key];
		if (property === null) {
			fields[key] = 'null';
		} else if (typeof property === "number") {
			fields[key] = Number.isInteger(property) ? 'integer' : 'real';
		} else {
			fields[key] = typeof property;
		}
	}

	var lastNames = [];

	for (var name in fields) {
		var type = types[fields[name]];
		if (type) {
			var shortName = name.substring(0, 10); //opcięcie do 10 znaków
			if (lastNames.includes(shortName)) {
				var i = 1;
				var shortName2 = `${name.substring(0, i < 10 ? 9 : 8)}${i}`;
				while (lastNames.includes(shortName2)) {
					i++;
					shortName2 = `${name.substring(0, i < 10 ? 9 : 8)}${i}`;
				}
				shortName = shortName2;
			}
			lastNames.push(shortName);

			o.push({
				name,
				shortName: iconv.encode(shortName, encode), //konversja z utf-8 do ascii
				type,
				size: fieldSize[type]
			});
		}
	}
	return o;
}

/**
 * @param {Array} fields
 * @returns {Array}
 */
function bytesPer(fields) {
	// deleted flag
	return fields.reduce(function (memo, f) {
		return memo + f.size;
	}, 1);
}
